<?php

// Sizing & Color constants.
$GLOBALS['supportedversions'] = [
	/**
	 * Version de l'API pour le script spip_loader.php
	 */
	'spip_loader_api_default' => 2,
	/**
	 * Partie mise en forme du calendrier au format SVG en pixels.
	 *
	 * Cf. modeles/supportedversions_calendar.html
	 */
	'svg' => [
		'margin_left' => 80,
		'margin_right' => 50,
		'header_height' => 24,
		'year_width' => 120,
		'branch_height' => 30,
		'footer_height' => 24,
	],
	/**
	 * Partie cosmétique pour CSS.
	 *
	 * Couleurs par défaut servant à mettre en évidence l'état de maintenance d'une version
	 * Ce sont des couleurs atténuées par rapport à celles utilisées sur les sites php.net ou symfony.com
	 */
	'colors' => [
		'future' => [
			'background' => '#edcbdc',
		],
		'stable' => [
			'background' => '#c3e2c5',
		],
		'security' => [
			'background' => '#fceaae',
		],
		'eol' => [
			'background' => '#930000',
			'color' => 'white',
		],
	],
	/**
	 * Partie servant à définir la longueur du calendrier en années.
	 *
	 * @see https://www.php.net/manual/fr/datetime.formats.php
	 *
	 * min_year : intervalle en nombre d'années à afficher avant l'année courante
	 * max_year : intervalle en nombre d'années à afficher à partir de l'année courante
	 */
	'calendar' => [
		'min_year' => 'P3Y',
		'max_year' => 'P5Y',
	],
];
