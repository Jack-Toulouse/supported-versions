<?php

$GLOBALS[$GLOBALS['idx_lang']] = [
	'branch' => 'Branche',
	'active_support' => 'Maintenance active',
	'active_support_definition' => 'Une version qui est activement prise en charge.
		Les bogues signalés et les problèmes de sécurité sont corrigés
		et des mises à jour régulières sont effectuées.',
	'active_support_until' => 'Maintenance active jusqu’au',
	'security_fix' => 'Correctifs de sécurité seulement',
	'security_fix_definition' => 'Une version prise en charge uniquement pour les problèmes
		de sécurité critiques.
		Les publications ne sont effectuées qu’au besoin.',
	'security_support_until' => 'Correctifs de sécurité jusqu’au',
	'end_of_life' => 'Fin de vie',
	'end_of_life_definition' => 'Une version qui n’est plus prise en charge.
		Les personnes utilisant cette version doivent mettre à jour dès que possible,
		car elles peuvent être exposées à des vulnérabilités de sécurité
		non corrigées.',
	'initial_release' => 'Première publication',
	'unreleased' => 'Non publiée',
	'unreleased_definition' => 'Une version qui n’a pas encore été publiée.',
	'php_compatibility' => 'Compatibilité PHP',
	'last_release' => 'Dernière publication',
	'latest_releases' => 'Dernières versions',
	'current_page' => 'version maintenue',
	'eol_page' => 'Un tableau de la fin de vie des branches est disponible.',
	'released_at' => 'Publiée le',
	'announcement' => 'Annonce',
	'changelog' => 'Changelog',
	'download' => 'Téléchargement',
	'download_size' => 'Taille',
	'freespace' => 'Espace disque (hors base de données)',
	'ram' => 'Mémoire RAM',
	'system_needs'  => 'Besoin système minimum',
	'sql' => 'Base de données',
	'image_processing' => 'Traitement d’images',
	'required' => 'Requis',
	'suggest' => 'Suggestions',
	'provided' => 'Fourni',
	'php_extensions' => 'Extensions PHP',
	'no_future_version' => 'Pas de future version prévue actuellement.',
	'no_maintained_version' => 'Pas de version maintenue actuellement.',
];
