<?php

namespace JamesRezo\SupportedVersions\Test;

use DateTime;
use PHPUnit\Framework\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    protected $now = '1971-11-05 18:40:12.345678Z';

    protected $calendarConfig = ['calendar' => ['min_year' => 'P1Y', 'max_year' => 'P1Y']];

    protected $svgConfig = [
        'svg' => [
            'margin_left' => 100,
            'margin_right' => 100,
            'year_width' => 256,
            'header_height' => 48,
            'branch_height' => 64,
        ],
    ];

    protected function tearDown(): void
    {
        SupportedVersions::unsetConfig();
    }
}
