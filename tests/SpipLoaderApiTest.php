<?php

namespace JamesRezo\SupportedVersions\Test;

/**
 * @covers \SupportedVersions
 */
class SpipLoaderApiTest extends TestCase
{
	public function dataSpipLoaderApi()
	{
		return [
			'No parameter' => [
				0,
				$this->calendarConfig,
			],
			'Malformed parameter' => [
				0,
				['spip_loader_api_default' => 'test'],
			],
			'Nominal parameter' => [
				1,
				['spip_loader_api_default' => 1],
			],
		];
	}

	/**
     * @group api
     * @dataProvider dataSpipLoaderApi
	 */
	public function testSpipLoaderApi($expected, $config)
	{
		//Given
        SupportedVersions::setConfig(
            $config,
        );

		//When
		$actual = SupportedVersions::spipLoaderApi();

		//Then
		$this->assertEquals($expected, $actual);
	}

	public function dataGetMTime()
	{
		$timestamp = stat(__DIR__ . '/releases/init.json')['mtime'];
		return [
			'found' => [$timestamp, __DIR__ . '/releases/init.json'],
			'not found' => [0, ''],
		];
	}

	/**
	 * @covers ::get_mtime
	 * @dataProvider dataGetMTime
	 *
	 * @return void
	 */
	public function testGetMTime($expected, $filename)
	{
		// Given

		// When
		$actual = get_mtime($filename);

		// Then
		$this->assertEquals($expected, $actual);
	}

	public function dataGetLastModified()
	{
		return [
			'releases' => ['Last-Modified: Fri, 05 Nov 1971 18:40:12 GMT', '1970-11-05 21:00:00.000000Z'],
			'preReleases' => ['Last-Modified: Wed, 01 Dec 1971 21:00:00 GMT', '1971-12-01 21:00:00.000000Z'],
		];
	}

	/**
	 * @covers ::last_modified
	 * @dataProvider dataGetLastModified
	 *
	 * @return void
	 */
	public function testGetLastModified($expected, $preReleases)
	{
		// Given
		$releases = \DateTime::createFromFormat('Y-m-d H:i:s.uZ', $this->now, new \DateTimeZone('UTC'))->format('U');

		// When
		$preReleases = \DateTime::createFromFormat('Y-m-d H:i:s.uZ', $preReleases, new \DateTimeZone('UTC'))->format('U');
		$actual = last_modified($releases, $preReleases);

		// Then
		$this->assertEquals($expected, $actual);
	}
}
